import torch
from tqdm import tqdm
import numpy as np

from src import *

from torchvision import models


resnet = models.resnet50(pretrained=True)
resnet.eval()


tensor = read_image("data/tennis.jpg")
arr = to_array(tensor)


jac = gen_jacobian(resnet, tensor)
torch.save(jac, 'data/gen/tennis-jac.pt')


# check if the two methods generate the same jacobian:
# grads = torch.zeros(1000, 3, 224, 224)
# for target in tqdm(range(1000)):
#     grads[target] = compute_inputgrad(tensor, resnet, target)
# torch.save(grads, 'data/gen/dog-grads.pt')
#
# A = torch.load('data/gen/dog-grads.pt')
#
# jac = torch.load('data/gen/dog-jac.pt')
#
# print(A.view(1000,3*224*224))
# print(jac.view(1000,3*224*224))
# print(A.view(1000,3*224*224)[:300]==jac.view(1000,3*224*224)[:300])
