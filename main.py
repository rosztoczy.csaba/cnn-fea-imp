import numpy as np

import matplotlib.pyplot as plt

from PIL import Image

import torch
import torch.nn as nn
import torch.optim as optim

from torchvision.transforms import (CenterCrop, Compose, Normalize, Resize,
                                    ToTensor)

from tqdm import tqdm

from src import *

from torchvision import models


resnet = models.resnet50(pretrained=True)

resnet.eval()

tensor = read_image("data/flamingos.jfif")
arr = to_array(tensor)

with open('data/imagenet_classes.txt') as f:
    labels = [line.strip() for line in f.readlines()]

out = resnet(tensor)
_, index = torch.max(out, 1)

percentage = torch.nn.functional.softmax(out, dim=1)[0] * 100

print(index[0], labels[index[0]], percentage[index[0]].item())

_, indices = torch.sort(out, descending=True)
print(*[(idx, labels[idx], percentage[idx].item()) for idx in indices[0][:5]],
      sep='\n')

inp_grad = compute_inputgrad(
    tensor, resnet, index[0]
)

inp_grad_scaled = scale_grad(inp_grad)
print(inp_grad.shape)
inmp = scale_grad(inp_grad, mean=False)

# jac = gen_jacobian(resnet, tensor)
jac = torch.load('data/gen/flamingos-jac.pt')

std = compute_std(jac, reshape=True)
std_scaled = scale_grad(std.unsqueeze(0))

cov = compute_cov(jac, reshape=True)
cov_scaled = scale_grad(cov.unsqueeze(0))

print(std.shape)

std_ss = get_sil(torch.tensor(std_scaled).permute(2,0,1), r=1)

g = rgb2gray(arr)
print(g.shape)