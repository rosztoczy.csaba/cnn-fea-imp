import numpy as np
import matplotlib.pyplot as plt
from PIL import Image

import torch
import torch.nn as nn
import torch.optim as optim

from torchvision.transforms import (CenterCrop, Compose, Normalize, Resize,
                                    ToTensor)

from tqdm import tqdm


def get_sil(S, r, theta=3, sumcol=True):
    """Computes Silhouette map
    ----------
    S : torch.Tensor
        Importance map.
    r : int
        neighbor size
    theta : float
        threshold
    sumcol : bool
        If true, than all color values are preserved if one meets the conditions
    Returns
    -------
    sil : torch.Tensor
        Silhouette map
    """
    s = S.shape
    print(s)
    ws = 2 * r + 1    # actual window size
    Sp = nn.functional.pad(S, (r, r, r, r), mode='reflect')

    Ns = nn.functional.unfold(Sp, kernel_size=ws)
    Ns = Ns.view(s[0], ws, ws, s[1] * s[2]).permute(0, 3, 1, 2)

    S_rep = S.repeat_interleave(ws*ws, dim=2).view(s[0], s[1]*s[2], ws, ws)

    mask = Ns / S_rep >= theta
    sil = S.clone()

    if sumcol:
      mask = mask.sum((0,2,3)).view(s[1],s[2]).to(bool)
      sil[:, ~mask] = 0
    else:
      mask = mask.sum((2,3)).view(s[0],s[1],s[2]).to(bool)
      sil[~mask] = 0
    # print(sil.shape)
    return sil
