import numpy as np
import matplotlib.pyplot as plt
from PIL import Image

import torch
import torch.nn as nn
import torch.optim as optim

from torchvision.transforms import (CenterCrop, Compose, Normalize, Resize,
                                    ToTensor)

from tqdm import tqdm


def compute_gradient(func, inp, **kwargs):
    """Compute the gradient with respect to `inp`.
    Parameters
    ----------
    func : callable
        Function that takes in `inp` and `kwargs` and returns a single element
        tensor.
    inp : torch.Tensor
        The tensor that we want to get the gradients for. Needs to be a leaf
        node.
    **kwargs : dict
        Additional keyword arguments passed into `func`.
    Returns
    -------
    grad : torch.Tensor
        Tensor of the same shape as `inp` that is representing the gradient.
    """
    inp.requires_grad = True

    loss = func(inp, **kwargs)
    loss.backward()

    result = inp.grad.data.clone()
    inp.requires_grad = False
    inp.grad.data.zero_()
    return result


def func(inp, net=None, target=None):
    """Get logit of a target class.
    Parameters
    ----------
    inp : torch.Tensor
        Input image (single image batch).
    net : torch.nn.Module
        Classifier network.
    target : int
        Imagenet ground truth label id.
    Returns
    -------
    logit : torch.Tensor
        Logit of the `target` class.
    """
    out = net(inp)
    logit = out[0, target]

    return logit


def compute_inputgrad(inp, net, target):
    """Computes input*grad.
    Parameters
    ----------
    inp : torch.Tensor
        Input image (single image batch) of shape `(1, 3, *, *)`.
    net : torch.nn.Module
        Classifier network.
    target : int
        Imagenet ground truth label id.
    Returns
    -------
    inp_grad : torch.Tensor
        Gradient with respect to the `inp` tensor. Same shape as `inp`.
    """
    grads = compute_gradient(func, inp, net=net, target=target)

    return grads


def compute_var(A, reshape=False):
    """Computes variation based feature importance from attribution maps.
    Parameters
    ----------
    A : torch.Tensor
        Attribution map.
    reshape : bool
        If True, then the feature importances are reshaped.
    Returns
    -------
    var : torch.Tensor
        Variation based feature importance of the feature vector.
    """
    X = A.clone()
    s = X.shape
    k = s[-4]
    X = X.reshape(s[-4], s[-3] * s[-2] * s[-1])
    Xc = X.sum(0)
    var = (X - 1 / k * Xc[None, :]) ** 2

    var = torch.sum(var, axis=0)

    if reshape:
        var = var.reshape(s[-3:])

    return var


def compute_std(A, reshape=False):
    """Computes STD based feature importance of attribution maps.
    Parameters
    ----------
    A : torch.Tensor
        Attribution map.
    reshape : bool
        If True, than the feature importances are reshaped.
    Returns
    -------
    std : torch.Tensor
        STD based feature importance of the feature vector.
    """
    var = compute_var(A, reshape=reshape)

    std = torch.sqrt(var)

    return std


def compute_cov(A, reshape=False):
    """Computes covariation based feature importance from attribution maps.
    Uses alternate computation method, instead of ((X-1/k*X.sum(0)[None, :]).t() @ (X-1/k*X.sum(0)[None, :])).sum(0).
    Parameters
    ----------
    A : torch.Tensor
        Attribution map.
    reshape : bool
        If True, then the feature importances are reshaped.
    Returns
    -------
    phi : torch.Tensor
        Covariation based feature importance of the feature vector.
    """
    X = A.clone()
    s = X.shape
    k = s[-4]
    X = X.reshape(s[-4], s[-3] * s[-2] * s[-1])
    Xc = X.sum(0)
    Xr = X.sum(1)
    cov = (X - 1 / k * Xc[None, :]) * ((Xr[:, None] - 1 / k * X.sum()) - (X - 1 / k * Xc[None, :]))

    phi = torch.sum(cov, axis=0)

    if reshape:
        phi = phi.reshape(s[-3:])

    var = compute_var(A, reshape=reshape)
    phi = var + phi

    return phi


def gen_jacobian(func, inputs, batched=False):
    """Generates the input-based jacobian tensor.
    Parameters
    ----------
    func : callable
        Function that takes inputs and gives back a single value or vector.
    inputs : torch.Tensor
    batched : bool
        If False, then batch dimensions are flattened.
    Returns
    -------
    jac : torch.Tensor
        The jacobian tensor.
    """
    jac = torch.autograd.functional.jacobian(func, inputs)
    if not batched:
        s = np.array(jac.shape)
        jac = jac.view(tuple(s[np.greater(s, 1)]))
    return jac
