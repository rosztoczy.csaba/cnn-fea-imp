import numpy as np
import matplotlib.pyplot as plt
from PIL import Image

import torch
import torch.nn as nn
import torch.optim as optim

from torchvision.transforms import (CenterCrop, Compose, Normalize, Resize,
                                    ToTensor)

from tqdm import tqdm


def read_image(path):
    """Load image from disk and convert to torch.Tensor.
    Parameters
    ----------
    path : str
        Path to the image.
    Returns
    -------
    tensor : torch.Tensor
        Single sample batch containing our image (ready to be used with
        pretrained networks). The shape is `(1, 3, 224, 224)`.
    """
    img = Image.open(path)

    transform = Compose([Resize(256),
                         CenterCrop(224),
                         ToTensor(),
                         Normalize(mean=[0.485, 0.456, 0.406],
                                   std=[0.229, 0.224, 0.225])])

    tensor_ = transform(img)
    tensor = tensor_.unsqueeze(0)

    return tensor


def rgb2gray(rgb):
    """Convert rgb image to grayscale. (Uses the matlab conventions.)
    Parameters
    ----------
    rgb : np.ndarray
        Array of shape `(*, *, 3)` representing an rgb image.
    Returns
    -------
    arr : np.ndarray
        Array of shape `(*, *)` representing a grayscale image.
    """
    return np.dot(rgb[...,:3], [0.2989, 0.5870, 0.1140])


def to_array(tensor):
    """Convert torch.Tensor to np.ndarray.
    Parameters
    ----------
    tensor : torch.Tensor
        Tensor of shape `(1, 3, *, *)` representing one sample batch of images.
    Returns
    -------
    arr : np.ndarray
        Array of shape `(*, *, 3)` representing an image that can be plotted
        directly.
    """
    tensor_ = tensor.squeeze()

    unnormalize_transform = Compose([Normalize(mean=[0, 0, 0],
                                               std=[1 / 0.229, 1 / 0.224, 1 / 0.225]),
                                     Normalize(mean=[-0.485, -0.456, -0.406],
                                               std=[1, 1, 1])])
    arr_ = unnormalize_transform(tensor_)
    arr = arr_.permute(1, 2, 0).detach().numpy()

    return arr


def scale_grad(grad, mean=True):
    """Scale gradient tensor.
    Parameters
    ----------
    grad : torch.Tensor
        Gradient of shape `(1, 3, *, *)`.
    mean : bool
        If True then computes mean over color-channels.
    Returns
    -------
    grad_arr : np.ndarray
        Array of shape `(*, *, 1 or 3)`.
    """
    if mean:
        grad_arr = torch.abs(grad).mean(dim=1).detach().permute(1, 2, 0)
    else:
        grad_arr = torch.abs(grad).detach().squeeze().permute(1, 2, 0)
    grad_arr /= grad_arr.quantile(0.98)
    grad_arr = torch.clamp(grad_arr, 0, 1)

    return grad_arr.numpy()